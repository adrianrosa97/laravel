<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    use RefreshDatabase;

    /**
     * @group users
     *
     * @return void
     */
    public function testListaDeUsuariosVacia()
    {
        $response = $this->get('/users');
      $response->assertStatus(200);
      $response->assertSee('Laravel');
      $response->assertSee('No hay usuarios');
    }

    public function test_lista_de_usuarios()
    {
      factory(User::class)->create([
          'name' => 'Pepe',
          'email' => 'pepe@gmail.com'
      ]);

      factory(User::class, 100)->create();

      $response = $this->get('/users');
      $response->assertStatus(200);
      $response->assertDontSee('No hay usuarios');
      $response->assertSee('Laravel');
      $response->assertSee('Pepe');
      $response->assertSee('pepe@gmail.com');
    }

    // public function testListaDeUsuarios()
    // {
    //     $response = $this->get("/users");
    //     $response->assertStatus(200);
    //     $response->assertSee("Listado");
    //     // $response->assertSee("Javi");
    //     // $response->assertSee("Ana");
    // }

    public function test_metodo_show_user_1()
    {
      factory(User::class)->create([
          'id' => 1,
          'name' => 'Pepe',
          'email' => 'pepe@gmail.com'
      ]);

      $response = $this->get('/users/1');
      $response->assertStatus(200);
      $response->assertSee('Este es el detalle del usuario 1');
      $response->assertSee('Pepe');
      $response->assertSee('pepe@gmail.com');
    }

    public function test_metodo_show_user_inexistente()
{
    $response = $this->get('/users/100000');
    $response->assertStatus(404);
}
}
