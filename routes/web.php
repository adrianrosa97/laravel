<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/users', "UserController@index")->name("usuarios");
// Route::get('/users/create', "UserController@create");
// Route::get('/users/{id}', "UserController@show");
Route::get('/', function () {
    return view("welcome");
});

Route::get('/users/especial', "UserController@especial");
Route::resource('users', "UserController");
Route::resource('products', "ProductController");
// Route::resource('users', "UserController",["except"=>["create"]]);


// Route::get('users/{id}', function ($id) {
//     return "Detalle del usuario $id";
// });

// Route::get('users/{id}/{name?}', function ($id,$name=null) {
//     if($name){
//         return "Detalle del usuario $id con nombre $name";
//     }else{
//         return "Detalle del usuario $id siendo anonimo";
//     }
// });