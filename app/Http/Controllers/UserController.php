<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //echo "index";
        //$users = User::all();
        $users = User::paginate();
        //$users = array("Javi", "Ana", "Diego", "Sandra");
        //$users = array();

        return view("user.index", ["users" => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("user.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validacion
        $request->validate([
            "nombre" => "required|max:255",
            "email" => "required|unique:users|email",
            "password" => "required|min:6"
        ]);


        //Opcion 1
        // $user = new User;
        // $user->name = $request->input("nombre");
        // $user->email = $request->input("email");
        // $user->password = bcrypt($request->input("password"));
        // $user->remember_token = str_random(10);
        // $user->save();

        //Opcion 2
        //La contraseña no se encripta
        //$user = User::create($request->all());

        //Opcion 3
        //La contraseña no se encripta
        $user = new User();
        $user->fill($request->all());
        $user->save();
        

        return redirect("/users");
        // dd($request->only("nombre","email"));
        // dd($request->all());
        //return "alta de usuario";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        
        //$user = User::findOrFail($id);
        //$otro = "<h3>otra variable</h3>";
        //dd(compact("id","otro"));
        //$user = User::find($id);
        if($user == null){
            abort(404);
        }
        return view("user.show", ["user" => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view("user.edit", ["user" => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validacion
        $request->validate([
            "name" => "required|max:255",
            "email" => "required|unique:users,email,$id,id|email"
        ]);
        $user = User::findOrFail($id);
        $user->fill($request->all());
        $user->save();
        return redirect("users/".$user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $user = User::findOrFail($id);
        // $user->delete();
        // return redirect("/users");

        User::destroy($id);
        return back();
    }

    public function especial()
    {
        $users = User::where("id",">",5)->where("id","<=", 10)->get();
        dd($users);
        //return view("/user/especial");
    }
}
