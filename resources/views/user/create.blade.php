@extends('layouts.index')

@section('title')
    <h1>Usuarios</h1>
@endsection
    
@section('content')
    <h1>Alta de usuarios</h1>
    {{-- @if ($errors->any())
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif --}}
    <form action="/users" method="POST">
        {{ csrf_field() }}

        <label for="nombre">nombre</label>
        <input type="text" name="nombre" value="{{ old("nombre")}}">
        <div style="color:red">
        {{ $errors->first("nombre") }}
    </div>
        <br>
        <label for="email">email</label>
        <input type="text" name="email" value="{{ old("email")}}">
        <div style="color:red">
            {{ $errors->first("email") }}
        </div>
        <br>
        {{-- @php
            $colores = ["rojo","azul","verde"];
        @endphp
        <select name="color">
            @foreach ($colores as $item)
                <option value="{{ $item }}" {{ old("color")  == $item ? 'selected="selected"' : "" }} >{{ $item }}</option>
            @endforeach
        </select> --}}
        <label for="password">contraseña</label>
        <input type="password" name="password">
        <div style="color:red">
            {{ $errors->first("password") }}
        </div>
        <br>
        <input type="submit" value="nuevo">
    </form>
@endsection