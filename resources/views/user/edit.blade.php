@extends('layouts.index')

@section('title')
<h1>Usuario {{ $user->name }}</h1>
@endsection
    
@section('content')
    <h1>Edicion del usuario {{ $user->name }}</h1>
    <form action="/users/{{ $user->id }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">

        <label for="nombre">nombre</label>
        <input type="text" name="name" value="{{ old("name") ? old("name") : $user->name }}">
        <div style="color:red">
            {{ $errors->first("name") }}
        </div>
        <br>
        <label for="email">email</label>
        <input type="text" name="email" value="{{ old("email") ? old("email") : $user->email }}">
        <div style="color:red">
            {{ $errors->first("email") }}
        </div>
        <br>
        {{-- <label for="password">contraseña</label>
        <input type="password" name="password">
        <br> --}}
        <input type="submit" value="editar">
    </form>
@endsection