@extends('layouts.index')

@section('title')
    <h1>Esta es la lista de usuarios</h1>
@endsection
    
@section('content')
    <a href="/users/create">Nuevo usuario</a>
    {{-- <ul>
    @foreach ($users as $user)
        <li>Nombre: {{$user->name}} | Email: {{$user->email}}</li>
    @endforeach
    </ul> --}}
    <hr>
    <h1>Esta es la lista de usuarios</h1>
    <ul>
        @if (!empty($users))
    @foreach ($users as $user)
    <li>Nombre: {{$user->name}} | Email: {{$user->email}} | <a href="/users/{{ $user->id }}/edit">Editar</a> | 
    <form action="/users/{{ $user->id }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit" value="borrar">
    </form>
    </li>
    @endforeach
    @else
        <p>No hay usuarios!!</p>
    @endif
    </ul>
    {{-- <hr>
    <h1>Esta el la lista de usuarios</h1>
    <ul>
    @forelse ($users as $user)
        <li>Nombre: {{$user->name}} | Email: {{$user->email}}</li>
    @empty
    </ul>
        <p>No hay usuarios!!</p>
    @endforelse --}}

    {{ $users->render() }}
@endsection
    
