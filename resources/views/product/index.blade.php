@extends('layouts.index')
@section("title","<h1>Lista de productos</h1>")

@section("content")
    <table>
    <tr>
    <th>Nombre</th>
    <th>Precio</th>
    <th>Acciones</th>
    </tr>
    @foreach ($products as $p)
        <tr>
        <td>{{$p->name}}</td>
        <td>{{$p->price}}€</td>
        <td><a href="/products/{{ $p->id }}">Ver</a></td>
        </tr>
    @endforeach
    </table>
@endsection