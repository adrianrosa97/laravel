<?php

use Illuminate\Database\Seeder;
use Faker\Generator as faker;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        DB::table("products")->insert([
            "name" => "banana",
            "price" => 3,
            "created_at" => now(),
            "updated_at" => now()
        ]);
        for($i = 0; $i < 50; $i++){
            DB::table("products")->insert([
                "name" => $faker->sentence(2,true),
                "price" => rand(100,2000)/100,
                "created_at" => now(),
                "updated_at" => now()
            ]);
        }
    }
}
